"""
Given a list and a number, create a new list that contains each number of list
at most N times, without reordering.

For example if the input number is 2, and the input list is [1,2,3,1,2,1,2,3],
you take [1,2,3,1,2], drop the next [1,2] since this would lead to 1 and 2 being
in the result 3 times, and then take 3, which leads to [1,2,3,1,2,3].

With list [20,37,20,21] and number 1, the result would be [20,37,21].
"""

order = [1, 1, 3, 3, 7, 2, 2, 2, 2, 4, 5, 6, 7, 76, 45, 32, 4, 4, 6, 3, 2]  # Given a list
max_e = 2  # maximum number of occurrences


def delete_nth(given_list, max_occurrence):
    new_order = []
    for item in given_list:  # add items from given list to new list
        if new_order.count(item) < max_occurrence:
            new_order.append(item)

    print(new_order)
    return new_order


delete_nth(order, max_e)
