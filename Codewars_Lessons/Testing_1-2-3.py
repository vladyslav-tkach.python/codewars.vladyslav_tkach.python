"""
Your team is writing a fancy new text editor, and you've been
tasked with implementing the line numbering.

Write a function which takes a list of strings and returns each line
prepended by the correct number.

The numbering starts at 1. The format is n: string. Notice the colon and space in between.

Examples: (Input --> Output)

[] --> []
["a", "b", "c"] --> ["1: a", "2: b", "3: c"]
"""
lines_example = ["a", "b", "c"]


def number(lines):
    numbers = list(range(1, int(len(lines))+1))
    dictio = dict(zip(numbers, lines))
    new_list = []

    for key, value in dictio.items():
        value = f"{key}: {value}"
        new_list.append(value)

    return new_list


print(number(lines_example))
