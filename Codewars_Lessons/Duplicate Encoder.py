"""
The goal of this exercise is to convert a string to a new string where each
character in the new string is "(" if that character appears only once in the
original string, or ")" if that character appears more than once in the original
string. Ignore capitalization when determining if a character is a duplicate.

"""

input_word = input("Enter the word: ")


def duplicate_encode(word):
    word = word.lower()
    new_word = ''.join(['(' if word.count(char) == 1 else ')' for char in word])

    print(new_word)


duplicate_encode(input_word)
