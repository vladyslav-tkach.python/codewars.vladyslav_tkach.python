"""
Your task is to convert strings to how they would be written by Jaden Smith.
The strings are actual quotes from Jaden Smith, but they are not capitalized
in the same way he originally typed them.

Example:
Not Jaden-Cased: "How can mirrors be real if our eyes aren't real"
Jaden-Cased:     "How Can Mirrors Be Real If Our Eyes Aren't Real"

Link to Jaden's former Twitter account @officialjaden via archive.org

"""

string = 'How can mirrors be real if our eyes aren\'t real'


def to_jaden_case(string):
    print(f'\nOld string: {string}')
    print(f'\nNew String: {" ".join([word.capitalize() for word in string.split(" ")])}')
    return " ".join([word.title() for word in string.split(" ")])


to_jaden_case(string)
